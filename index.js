const express = require('express')
var cors = require('cors')
var hbs = require('express-handlebars')
var path = require('path')
var exec = require('child_process').exec;
var fs = require("fs");
var settings = require("./config/settings.json");
var system = require("./config/system.json");

var app = express();
app.use(cors());

app.engine('hbs', hbs({
    extname: 'hbs', defaultLayout: 'layout',
    layoutsDir: __dirname + '/views/layouts/'
}));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.get('/', function (req, res) {
    res.send({ guide: "rules" });
});

app.get('/metrics', function (req, res) {
    var file = fs.readFileSync("config/metrics.json");
    var jsonMetrics = JSON.parse(file);

    var data = {};
    data.ip = settings.ip
    data.port = settings.port
    data.title = "Metrics"
    data["metrics"] = jsonMetrics.metrics
    console.log("Data:", data);

    res.render('metrics', data);
});

app.get('/api/metrics/alarmed', (req, res) => {
    // if (typeof req.query.id === 'undefined' ||
    //     req.query.id == "") {
    // }

    var cmd = "echo \"cli metric alarm " + req.query.id + " " + req.query.state + "\" >> ~/a.txt";
    console.log(cmd);
    exec(cmd, (err, stdout, stderr) => {
        if (err) {
            // node couldn't execute the command
            return;
        }

        console.log(`stdout: ${stdout}`);
        console.log(`stderr: ${stderr}`);
    });

    res.send({ command: cmd });
});

app.get('/system', function (req, res) {
    var data = {};
    data.ip = settings.ip
    data.port = settings.port
    data.title = "System"
    data.modes = system.modes
    console.log("Data:", data);

    res.render('system', data);
});

app.get('/api/system', (req, res) => {

    var cmd = "echo \"cli sysmode " + req.query.id + "\" >> ~/a.txt";
    console.log(cmd);
    exec(cmd, (err, stdout, stderr) => { });

    res.send({ command: cmd });
});

var server = app.listen(settings.port, function () {
    console.log('coreweb listening on port %s!', server.address().port)
})
